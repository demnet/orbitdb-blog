const IPFS = require("ipfs")
const OrbitDB = require("orbit-db")
const yargs = require('yargs');
const Database = require("./Database")
const fs = require("fs")

yargs.scriptName("blog")
     .usage("$0 <cmd> [args]")
     .command("pin [address]", "Pin a Database", args => {
       args.positional("address", {
         type: "string",
         describe: "Address of the OrbitDB Database to pin"
       })
     }, async argv => {
       const {ipfs, orbitdb} = await create()
       const db = Database.open(argv.address, ipfs, orbitdb)
       db.pin(db, ipfs)
       console.log(`Pinned ${argv.address}`)
       process.exit(0)
     })
     .command("create [name]", "Create a new database", args => {
       args.positional("name", {
         type: "string",
         describe: "Name of the new blog"
       })
     }, async argv => {
       const {ipfs, orbitdb} = await create()

       const db = await Database.create(argv.name, orbitdb, ipfs)
       console.info(db.toString())
       process.exit(0)
     })
     .command("add [address] [title] [content]", "Add content", args => {
       args.positional("address", {
         type: "string",
         describe: "Address of the Database to add to"
       })

       args.positional("title", {
         type: "string",
         describe: "Title of the content"
       })

       args.positional("content", {
         type: "string",
         describe: "File with the content to add to the blog"
       })
     }, async argv => {
       const {ipfs, orbitdb} = await create()
       const db = await Database.open(argv.address, orbitdb, ipfs)
       const content = fs.readFileSync(argv.content, { encoding: "utf-8" })

       let hash = await db.add({ title: argv.title, content: content })
       console.info(`Added ${argv.title} under ${hash}`)
       process.exit(0)
     })
     .help()
     .argv

async function create() {
  const ipfs = await IPFS.create({ repo: "../jsipfs", silent: true })
  const orbitdb = await OrbitDB.createInstance(ipfs, { directory: "../orbitdb"})
  return { ipfs: ipfs, orbitdb: orbitdb }
}
