let contentElem = document.getElementById("content")

document.addEventListener('DOMContentLoaded', async () => {
  const ipfs = await Ipfs.create()
  const orbitdb = await OrbitDB.createInstance(ipfs)
  const address = parse_query().address

  const db = await Database.open(address, orbitdb)
  contentElem = viewIndex(db)
})

function viewIndex(db) {
  let md = ""

  db._db.iterator({ limit: -1 })
        .collect()
        .forEach(entry => {
          md += `- [${entry.payload.title}](ipfs.io/ipfs/{entry.payload.cid})\n`
        })

  contentElem.innerHTML = marked(md);
}

/**
 * parse query string
 * @param keys {?Array[string]} to expect in query
 */
function parse_query(keys = null) {
  let search = window.location.search.substring(1)
  search = search.split("&")
  let result = {}

  let queryKeys = []
  for (let pair of search) {
    let index = pair.indexOf("=")
    let key = pair.slice(0, index)
    let value = pair.slice(index+1)
    result[key] = value
    queryKeys.push(key)
  }

  if(keys == null) {
    return result
  } else {
    return [result, keys.every(v => queryKeys.indexOf(v) != -1)]
  }
}
