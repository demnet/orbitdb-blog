class Database {
  constructor(db, orbitdb, ipfs) {
    this._db = db
    this._orbitdb = orbitdb
    this._ipfs = ipfs
  }

  static async create(name, orbitdb, ipfs) {
    const db = await orbitdb.feed(name, {
      accessController: {
        type: "orbitdb",
        write: [orbitdb.identity.id]
      }
    })
    return new Database(db, orbitdb, ipfs)
  }

  static async open(address, orbitdb, ipfs) {
    const db = await orbitdb.feed(address)
    return new Database(db, orbitdb, ipfs)
  }

  async pin() {
    this._db.iterator({ limit: -1 }).collect().forEach(entry => {
      ipfs.pin.add(entry.payload.cid)
    })
  }

  async add(post) {
    const {cid} = await this._ipfs.add(JSON.stringify(post.content))
    return await this._db.add({
      title: post.title,
      cid: cid,
      time: Date.now()
    })
  }

  toString() {
    return this._db.address.toString()
  }
}
